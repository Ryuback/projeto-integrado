<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDBProjetoIntegrado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('escola', function (Blueprint $table) {
            $table->id();
            $table->text('nome');
            $table->bigInteger('qtd_denuncia');	
            $table->timestamps();
        });
        
        Schema::create('tipo_bullying', function (Blueprint $table) {
            $table->id();
            $table->text('tipo');
            $table->timestamps();
        });

        Schema::create('denuncia', function (Blueprint $table) {
            $table->id();
            $table->text('nome');
            $table->text('cpf');
            $table->unsignedBigInteger('escola_id');
            $table->foreign('escola_id')->references('id')->on('escola');
            $table->unsignedBigInteger('tipo_bullying');
            $table->foreign('tipo_bullying')->references('id')->on('tipo_bullying');
            $table->longText('obs')->nullable();
            $table->timestamps();
        });

                // INSERT AFTER MIGRATION
                DB::table('tipo_bullying')->insert(array('tipo' => 'Físico',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Verbal',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Material',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Moral ou Sentimental',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Psicológico',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Sexual',));
                DB::table('tipo_bullying')->insert(array('tipo' => 'Virtual ou Cyberbullying',));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncia');
    }
}
