<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/input.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <!-- JS -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
    <title>Document</title>
</head>

<body>

    <div class="container-menu">
        <center>
            <div class="color-top-bar"></div>
            <div class="menu-top-bar">
                <a href="" class="menu-item">Início</a>
                <a href="#Sobre" class="menu-item">Sobre o Bullying</a>
                <a href="" class="menu-item">Tipos de Bullying</a>
                <a class="menu-item" id="myBtn">Faça sua Denúncia</a>
            </div>
        </center>
        <center><img class="img-menu" src="img/menu-image.png" alt="bullying-image"
                style=" position:relative; top: 50px;"></center>
    </div>
    <div class="container">
        <div class="container-news">
            <div class="container-news-top">
                <div class="title-news" id="Sobre">Bullying no DF</div>
            </div>
            <div class="text-news">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
            </div>
        </div>

        <div class="container-news">
            <div class="container-news-top">
                <div class="title-news" id="Sobre">Freqência do Bullying nas Escolas</div>
            </div>
            <canvas id="myChart"></canvas>
        </div>

    </div>

    <br><br>

    <div class="container shadow"
        style="background-color: #03445E; opacity: 0.9; height: auto; padding:50px 0 50px 0 ; margin: 0;">

        <div class="container-news">
            <div class="container-news-top">
                <div class="title-news" id="Sobre">Frequência dos Tipos de Bullying</div>
            </div>
            <canvas id="myChart2"></canvas>
        </div>

        <div class="container-news">
            <div class="container-news-top">
                <div class="title-news" id="Sobre">Sobre o Bullying</div>
            </div>
            <div class="text-news">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
            </div>
        </div>

    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="title-news" style="position: relative; color: black;">Denúncia</div>
            <center>
                <hr class="hr-title">
            </center>

            <div class="form-style-5">

                {!! Form::open(['route' => 'projeto.store', 'method' => 'POST']) !!}

                <fieldset>
                    <legend><span class="number">1</span> Dados Pessoais</legend>
                    <center>

                        <input type="text" name="nome" placeholder="Seu nome *">
                        <input type="text" name="cpf" placeholder="Seu CPF *" id="CPF" maxlength="14">
                        <label for="school">Selecione sua escola:</label>
                        <input id="school" type="text" name="escola" placeholder="Digite o nome de sua Escola">
                        <label for="job">Tipo de Bullying:</label>
                        <select name="tipo_bullying">
                            <optgroup label="Tipo de Bullying">
                                @foreach($type as $types)
                                <option value="{{$types->id}}">{{$types->tipo}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </center>
                </fieldset>
                <fieldset>
                    <legend><span class="number">2</span> Sobre o Acontecimento</legend>
                    <center>
                        <textarea name="obs" placeholder="About Your School" style="height: 100px; resize:none;" ></textarea>
                    </center>
                </fieldset>
                <input type="submit" value="Enviar Denúncia" />
                {!! Form::close()!!}
            </div>
        </div>
</body>

@foreach($escola as $escolas)
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: [
                @foreach($escola as $escolas)
                '{{$escolas->nome}}',
                @endforeach
            ],
            datasets: [{
                label: 'Escolas',
                backgroundColor: '#1D3741',
                borderColor: 'rgb(255, 255, 255)',
                data: [
                    @foreach($escola as $escolas)
                    '{{$escolas->qtd_denuncia}}',
                    @endforeach
                ]
            }]
        },

        // Configuration options go here
        options: {}
    });
    var ctx = document.getElementById('myChart2').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [
                @foreach($type as $tipos_bullying)
                '{{$tipos_bullying->tipo}}',
                @endforeach
            ],
            datasets: [{
                label: 'Tipos',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [
                    @foreach($type as $tipos_bullying)
                '{{App\Denuncia::where('tipo_bullying',$tipos_bullying->id)->count()}}',
                    @endforeach
                ]
            }]
        },

        // Configuration options go here
        options: {}
    });

    window.onload = function () {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = Chart.Line(ctx, {
            data: lineChartData,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                title: {
                    display: true,
                    text: 'Chart.js Line Chart - Multi Axis'
                },
                scales: {
                    yAxes: [{
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                    }, {
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',

                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }],
                }
            }
        });
    };

</script>
@endforeach
<script>
    // Get the modal
    var modal = document.getElementById("myModal");
    modal.style.transition = " 2s";

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function () {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

</script>

<script>
    $(function () {
        var availableTags = [

            @foreach($escola as $escolas)
            '{{$escolas->nome}}',
            @endforeach

        ];
        $("#school").autocomplete({
            source: availableTags
        });
    });
</script>

<script>
        $(document).ready(function () { 
        var $seuCampoCpf = $("#CPF");
        $seuCampoCpf.mask('000.000.000-00', {reverse: true});
    });
</script>

</html>
