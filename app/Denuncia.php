<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    protected $table ='denuncia';
    protected $fillable = ['nome','cpf','escola','tipo_bullying','obs'];

    public function type()
    {
        return $this->hasOne('App\Tipo_bullying');
    }

    public function escola()
    {
        return $this->hasMany('App\Escola','id');
    }

}
