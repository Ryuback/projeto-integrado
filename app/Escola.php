<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{
    protected $table ='escola';
    protected $fillable = ['nome','qtd_denuncia'];
}
