<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_bullying extends Model
{
    protected $table ='tipo_bullying';
    protected $fillable = ['tipo'];
}
