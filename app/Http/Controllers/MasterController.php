<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Escola;
use App\Denuncia;
use App\Tipo_bullying;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escola = Escola::orderBy('qtd_denuncia')->paginate(10);
        $type = Tipo_bullying::paginate(50);
        $denuncia = Denuncia::paginate(10);
         return view('projeto.index', compact('escola','type','denuncia'))->with('i', (request()->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $escola = $request->escola;
        $id_escola = Escola::where('nome','=',$escola)->get();
        foreach ($id_escola as $escolas_id){
            $request->escola = $escolas_id->id ;
            $nome_escola = $escolas_id->nome;
       }

       Denuncia::insert([
        [ 'nome' => $request->nome,
         'cpf' => $request->cpf,
         'escola_id' => $request->escola,
         'tipo_bullying' => $request->tipo_bullying,
         'obs' => $request->obs],
    ]);
        


        $escola = Escola::find($request->escola);
        $update = Escola::find($request->escola)->update(['qtd_denuncia' => $escola->qtd_denuncia + 1]);

        return redirect()->route('projeto.index')->with('success', 'Services updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
